# explainer #


## Code ##

This includes a few scripts to fetch/format data and an example of how to build a simple prediction model in Python/Pandas. The gists used previously won't work, this code will. Am happy to take pull requests.

## Guides ##

A tutorial on how to learn Python and some notes on how to build applications in the area of sports modelling. This is still a work-in-progress and I am going to keep adding to this. Am happy to take pull requests here too.
